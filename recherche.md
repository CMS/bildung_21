# Quellen

https://www.ccc.de/schule
 * https://schule.muc.ccc.de/lehrerfortbildung
 * https://schule.muc.ccc.de/Lehrerfortbildung#tracking_im_web
 * https://berlin.ccc.de/page/cms/
     * WS Wie funktioniert das Netz? -> 3.-7.Klasse 90min
     * WS Spuren im Netz -> >6.Klasse 90min
     * [Digital Defenders](https://edri.org/files/defenders_v_intruders_de_web.pdf)
     * https://myshadow.org/
     * https://linus-neumann.de/2010/12/cc-video-der-digitale-briefumschlag/
         * https://vimeo.com/17611458
     * https://www.csunplugged.org/de/  CS Unplugged ist eine Sammlung kostenloser Lehrmaterialien, durch die Informatik anhand von anregenden Spielen und Aufgaben mit Karten, Bindfaden, Wachsstiften und viel Herumrennen gelehrt wird. 
     * https://htwins.net/scale2/ Webapplikation - comichaft von kleinsten Teilchen bis größten Objekten im Universum
     * https://github.com/Binary-Kitchen/SolderingTutorial/tree/master Lötbausatz vom Hack Space Regensburg / Wäscheklammer mit den vier blinkenden LEDs
     * https://jugendhackt.org/
     * TOCHECK
         * https://media.ccc.de/v/34c3-9194-bildung_auf_dem_weg_ins_neuland 
         * https://media.ccc.de/v/33c3-8262-zwischen_technikbegeisterung_und_kritischer_reflexion_chaos_macht_schule
         * https://www.bpb.de/lernen/digitale-bildung/werkstatt/
         * https://cre.fm/cre189-chaos-macht-schule
 * https://www.hackerspace-bamberg.de/Chaos_macht_Schule
     * https://www.hackerspace-bamberg.de/Otto_LC  Laser/Holz-Roboter
 * https://chaospott.de/cms.html
     * https://media.ccc.de/v/petit-foo-cms   CmS auf der Playcom  
     * Stammdatenspiel. Medien Monster
     * LED-Stern Workshop. Chaospott 
 * https://cms.hamburg.ccc.de/
 * https://hannover.ccc.de/projekte/schule/
 * Karlsruhe https://entropia.de/%C3%9Cbersicht_%C3%BCber_Vortr%C3%A4ge
     * https://entropia.de/Vorlage:Vergangene_Termine
     * https://entropia.de/%C3%9Cbersicht_%C3%BCber_Vortr%C3%A4ge <<<<<
     * 
 * ...

ohne relvante info
 * https://aachen.ccc.de/vortraege/
 * 

# TOCHECK

 * https://gnulinux.ch/ciw086-podcast  OSOS-Austria
    * https://linux-bildung.at/osos-austria/
    * https://linux-bildung.at/osos-projekte/
    * https://digitalcourage.de/
    * https://lernenwiedieprofis.ch/
    * https://unsere-digitale.schule/

    