[ NB: abgehakte Punkte sind irgendwie schon in [overview.md](./overview.md) mit eingeflossen ]

# Allgemeinbildende Schulen
- [ ] Lehrer:innen wechsel alle 2 Jahre?
- [ ] Qualitätskontrolle?
- [X] freie Lerninhalte (OER)
- [X] freie Software
- [X] Informatik als Interdiszliplinäres Fach
- [ ] verlängerung der Regelschulzeit
- [X] Technik-/Medienkompetenz durch Evolution...
... In der Grundschule das Gehäuse für ein Notebook bauen
... In der Oberschule die Hardware und Software
... offene Hardware mit freier Software
... welche Anforderungen muss das Gerät erfüllen?
- [X] diverseres Personal in Schule (Kooperation mit Wirtschaft)
- [ ] mehr Schulsozialarbeit
- [X] Schule als Ort des Lebens begreifen
- [X] Schule Nachmittags als Stadtteilhaus nutzen
- [X] Schule als Gesamtgesellschaftlichen Ort der Bildung betrachten
- [X] Dokumentation von Lernproblemen (bspw. Wiki)
    -> erstellung eigener Bildungsinhalte (Wikipedia-Prinzip)
- [X] Inklusive Schule denken
- [X] Stadtteil Schule möglich?
- [ ] Lernumgebung
    - [ ] Fachräume/ freie Lernräume
    - [ ] möglichst modular
    - [ ] Licht, Sitzmöglichkeiten, tische... Wie muss es sein damit es möglichst individuell anpassbar ist?

# Hardware & Software
- [ ] verpflichtend mind. open source, besser free software
- [ ] Investitionen in free Software Entwicklung
- [ ] eigene Hardware
    - Modular
    - erweiterbar
    - "wachsend"
    - open Hardware prinzip
    - Versicherung über Schul(-Träger)
    - mind. ein Ersatzgerät pro Schüler:In
    - Informatik in Verbindung mit Werken in der Grundstufe
        - bau dein Notebook selbst
    - Mittelstufe -> Erweiterung
    - Oberstufe -> weitere Erweiterung

## Hardware Ideen
- [Frame.work](https://frame.work/de/de)
- von einfach zu groß
    - Lampe mit led + batterie + schaltern
    - simple mp3-player mit battrie und button
    - simple Datenübertragung (sicher und unendlich weit) per serieller SST -> kabel schalter lampe
    - Operations-Verstärker (als mini cpu - Minus-Operation) -> Magnet-Kugel schweben lassen 
    - Simple-Roboter mit Programmierung per Tasten/ Stimme / App
        - Breadboard-Roboter, progammieren,erweiern ohne Programmierung
    - einfache "arduino" mikrocontroller mit graphischer Programierung
        - Entfernungsmessung mit Ultraschall
        - datalogging auf sd-card
- raspberrypi
- odroid h-reihe


